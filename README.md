# Codigo fuente de "Frases de Anime"

Frases de Anime es una aplicacion para Android en la que todos los días tendrás una nueva frase de una serie Anime o película en tus notificaciones

Podrás ver frases anteriores, marcar como favoritos, o compartirlas con tus amigos

## Requisitos para compilarla

Android Studio 3.0+

Kotlin

Anko (Debería descargarse automáticamente)

## Problemas al compilar la aplicación

La aplicación puede dar problemas de compilación al faltar las claves API de Admob (No subidas porque son propias, evitamos un mal uso de la API). En res/strings.xml hay unas de ejemplo, deberían valer

## Licencia

Mire el fichero LICENSE.md para detalles, usamos licencia GPL