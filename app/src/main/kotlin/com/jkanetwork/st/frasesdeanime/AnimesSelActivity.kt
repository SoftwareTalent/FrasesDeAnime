package com.jkanetwork.st.frasesdeanime

import android.content.Context
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import android.view.View
import android.widget.SimpleAdapter
import android.widget.ListView
import android.widget.TextView
import android.widget.Button
import org.jetbrains.anko.*
import org.jetbrains.anko.support.v4.UI
import org.jetbrains.anko.support.v4.find
import android.widget.Toast
import android.util.Log


import androidx.core.content.ContextCompat.startActivity
import android.content.Intent
import android.widget.AdapterView


/**
 * Created by kprkpr on 25/07/17.
 * Help: https://www.youtube.com/watch?v=x-M1L6tcsLo
 */
class AnimesSelActivity : AppCompatActivity(){
    val act = this /* Activity Context */

    /* For DSL bind */
    lateinit var view_list : ListView
    lateinit var btn_exit : Button
    lateinit var btn_sel : Button

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        var db = SQLiteHelper(applicationContext) //Database connection

        /** Layout in DSL */
        verticalLayout {
            linearLayout {
                btn_exit = button("Volver")
                btn_sel = button("Seleccionar todos").lparams(width = matchParent)
            }
            view_list = listView {
            }.lparams(width = matchParent, height = matchParent) {  }
        }


        if (db.getAllAnimeSel()){ //If all selected
            btn_sel.setText("Deseleccionar todos")
        }else{
            btn_sel.setText("Seleccionar todos")
        }

        //Load ids and Names
        val arrayIDs = db.getAllAnimeIDs()
        var arrayNames = db.getAllAnimeNames()
        val length = arrayIDs.size

        //Init List of objets
        var listSel: List<Boolean> = (emptyList())
        var listNames: List<String> = (emptyList())
        for (i in 0..length -1){ //Load selected for every one and create list names/selected
            listSel += listOf(db.getAnimeSel(arrayIDs[i]))
            listNames += listOf(arrayNames[i])
        }

        // Load adapter for view_list with an ANKO DSL style directy, and fill with data.
        // CustomListAdapter class is in "CustomListAdapter.kt"
        view_list.adapter = CustomListAdapter<String>({ listNames }) {
            index, listNames, view ->
            linearLayout {
                checkBox {
                    text = listNames[index]
                    setOnClickListener { db.setAnimeSel(db.getIDAnimeFromName(listNames[index])) }
                    setChecked(listSel[index])
                    textSize = 18f

                }.lparams(){topMargin = dip(6);bottomMargin = dip(6)}
            }
        }

        btn_sel.setOnClickListener() {
            db.setAllAnimeSel()
            restartActivity(act)
        }

        //"Return" to main
        btn_exit.setOnClickListener() {
            startActivity<MainActivity>()
        }

    }

    /** On back go to main */
    override fun onBackPressed() {
        startActivity<MainActivity>()
    }

}