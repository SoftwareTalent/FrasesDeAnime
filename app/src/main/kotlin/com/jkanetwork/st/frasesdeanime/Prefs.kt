package com.jkanetwork.st.frasesdeanime

import android.content.Context
import android.content.SharedPreferences

/**
 * Created by kprkpr on 18/07/17.
 * Help of: http://blog.teamtreehouse.com/making-sharedpreferences-easy-with-kotlin
 */
class Prefs (context: Context){
    var prefs: SharedPreferences = context.getSharedPreferences("com.jkanetwork.st.frasesdeanime.PREFS", 0);

    fun getHour(): Int {
        return prefs.getInt("HOUR", 8)
    }

    fun setHour(x: Int) {
        prefs.edit().putInt("HOUR", x).commit()
    }

    fun getMinute(): Int {
        return prefs.getInt("MINUTE", 0)
    }

    fun setMinute(x: Int) {
        prefs.edit().putInt("MINUTE", x).commit()
    }

    fun getNotifOn(): Boolean {
        return prefs.getBoolean("NOTIFON", true)
    }

    fun setNotifOn(b: Boolean) {
        prefs.edit().putBoolean("NOTIFON", b).commit()
    }

    fun getPersNotifOn(): Boolean {
        return prefs.getBoolean("PERSNOTIFON", false)
    }

    fun setPersNotifOn(b: Boolean) {
        prefs.edit().putBoolean("PERSNOTIFON", b).commit()
    }

    /** Esto devuelve true solo la primera vez */
    fun getFirstTime(): Boolean {
        if (prefs.getBoolean("FIRSTTIME",true) == true){
            prefs.edit().putBoolean("FIRSTTIME", false).commit()
            return true;
        }else{
            return false;
        }
    }

    //For countdown to rate us dialog
    fun getRateCountdown(): Int {
        return prefs.getInt("RATECOUNTDOWN", 6)
    }

    fun setRateCountdown() {
        prefs.edit().putInt("RATECOUNTDOWN", (getRateCountdown()-1)).commit()
    }

}