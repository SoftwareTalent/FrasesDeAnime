package com.jkanetwork.st.frasesdeanime

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import android.content.Context
import android.widget.ArrayAdapter
import org.jetbrains.anko.*
import kotlinx.android.synthetic.main.activity_opts.*
import android.content.Intent
import java.util.*
import android.content.BroadcastReceiver

/**
 * Created by kprkpr on 30/08/17.
 */
class OptsActivity : AppCompatActivity(){

    lateinit var P: Prefs

    val cont = this

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_opts)

        P = Prefs(applicationContext)

        val hours = ArrayList<Int>()
        val minutes = ArrayList<Int>()

        for (i in 0..23){
            hours.add(i)
        }
        for (i in 0..59){
            minutes.add(i)
        }

        var hoursAdapter = ArrayAdapter(this,android.R.layout.simple_list_item_1,hours)
        var minutesAdapter = ArrayAdapter(this,android.R.layout.simple_list_item_1,minutes)

        hoursAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        minutesAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)

        spinnerHour.adapter = hoursAdapter
        spinnerMinute.adapter = minutesAdapter

        spinnerHour.setSelection(P.getHour())
        spinnerMinute.setSelection(P.getMinute())

        chk_alarm.setChecked(P.getNotifOn())
        chk_persistent.setChecked(P.getPersNotifOn())

        setEnabledParts()

        /* Button listeners */
        chk_alarm.setOnClickListener() {
            setEnabledParts()
        }
        chk_persistent.setOnClickListener() {
            setEnabledParts()
        }

        btn_SaveNotif.setOnClickListener() {

            P.setHour(spinnerHour.getSelectedItem().toString().toInt())
            P.setMinute(spinnerMinute.getSelectedItem().toString().toInt())

            P.setNotifOn(chk_alarm.isChecked())
            P.setPersNotifOn(chk_persistent.isChecked())

            val nm = applicationContext.notificationManager
            nm.cancel(1); //Canel notif before service going on, for persistent notification problems
            putAlarm(this)
            if (chk_persistent.isChecked() && chk_alarm.isChecked()) {
                this.startService(Intent(this, AlarmNotif::class.java))
            }

        }
    }


    fun setEnabledParts() {
        if (chk_alarm.isChecked()) {
            chk_persistent.setEnabled(true)
            /* This other checks is chk_persistent checked dependent */
            if (chk_persistent.isChecked()) {
                spinnerHour.setEnabled(false)
                spinnerMinute.setEnabled(false)
            } else {
                spinnerHour.setEnabled(true)
                spinnerMinute.setEnabled(true)
            }
        } else {
            chk_persistent.setEnabled(false)
            spinnerHour.setEnabled(false)
            spinnerMinute.setEnabled(false)
        }
    }
}
