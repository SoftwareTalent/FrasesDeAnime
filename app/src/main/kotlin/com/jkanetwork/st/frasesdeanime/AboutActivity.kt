package com.jkanetwork.st.frasesdeanime

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import android.content.Context
import android.widget.TextView
import android.widget.ImageView
import android.view.View
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import org.jetbrains.anko.*

/**
 * Created by kprkpr on 30/08/17.
 */
class AboutActivity : AppCompatActivity(){

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        //Anko DSL Layout
        verticalLayout {
            textView{
                text = "¡Gracias por usar Frases de Anime! \nIdea por Alba García y desarrollada por JKANetwork!"
                textSize = 22f
                textColor = Color.BLACK
                textAlignment = View.TEXT_ALIGNMENT_CENTER
            }.lparams(width=wrapContent, height=wrapContent){padding= dip(12)}
            imageView{
                setImageResource(R.drawable.cerebro)
            }.lparams(width=matchParent, height=matchParent){}
            background = ColorDrawable(Color.parseColor("#F8F2F2"))
        }
    }

}
