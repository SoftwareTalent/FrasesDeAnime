package com.jkanetwork.st.frasesdeanime

import android.content.Context
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import android.content.Intent
import android.view.View
import android.widget.SimpleAdapter
import android.widget.ListView
import android.widget.TextView
import android.view.ContextMenu
import android.view.MenuItem
import android.widget.AdapterView
import android.content.ClipboardManager
import android.content.ClipData
import org.jetbrains.anko.*

//import kotlinx.android.synthetic.main.activity_simplelistview.*

/**
 * Created by kprkpr on 25/07/17.
 * Help: https://www.youtube.com/watch?v=x-M1L6tcsLo
 */
class FavsActivity : AppCompatActivity(){

    /* For DSL bind */
    lateinit var view_list : ListView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        /** Layout in DSL */
        verticalLayout {
            view_list = listView {
            }.lparams(width = matchParent, height = matchParent) {
            }
        }

        val data = ArrayList<HashMap<String,String>>()
        var db = SQLiteHelper(applicationContext)
        val listIDFrases = db.getAllIDFavs()
        val length = listIDFrases.size

        for (i in 0..length -1){
            val item = HashMap<String, String>()
            item.put("sentence",db.getSentenceFromIDF(listIDFrases[i]))
            item.put("anime",db.getAnimeFromIDF(listIDFrases[i]))
            data.add(item)
        }

        view_list.adapter = SimpleAdapter(this,data,android.R.layout.simple_list_item_2,
                        arrayOf("sentence","anime"), intArrayOf(android.R.id.text1,android.R.id.text2))

        registerForContextMenu(view_list)

    }

    override fun onCreateContextMenu(menu: ContextMenu, v: View,
                                     menuInfo: ContextMenu.ContextMenuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo)
        val inflater = getMenuInflater()
        inflater.inflate(R.menu.menu_favsentence, menu)
    }


    override fun onContextItemSelected(item: MenuItem): Boolean {
        val info = item.getMenuInfo() as AdapterView.AdapterContextMenuInfo
        val obj = view_list.getItemAtPosition(info.position) as HashMap<String, String>

        when (item.getItemId()) {
            R.id.copy -> {
                var clipboard = getSystemService(Context.CLIPBOARD_SERVICE) as ClipboardManager
                clipboard.setPrimaryClip(ClipData.newPlainText("Sentence", "${obj.get("sentence")} (${obj.get("anime")}) \n ${this.getString(R.string.sharefrom)}"))
                toast("Copiado al portapapeles")
                return true
            }
            R.id.delfav -> {
                var db = SQLiteHelper(applicationContext)
                val IDFavs = db.getAllIDFavs()
                db.setFav(IDFavs[info.position])
                restartActivity(this)
                return true
            }
            R.id.share -> {
                share("${obj.get("sentence")} (${obj.get("anime")}) \n ${this.getString(R.string.sharefrom)}")
                return true
            }
        }
        return false
    }



}